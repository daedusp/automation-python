"""
TestRail integration:
* limited to what we need at this time
* we assume TestRail operates in single suite mode
  i.e., the default, recommended mode

API reference: http://docs.gurock.com/testrail-api2/start
"""
import base64
import json
# import conf.testrailenv_conf as conf_file
import os  # dotenv, os
import sys
import urllib.error
import urllib.request
import datetime
from utils import exceptions_handler as eh, logger as log


url, user, password = "", "", ""


def set_tr_config(context):
    global url, user, password
    """Set the TestRail URL and username, password"""
    tr_url = context.config.userdata.get('tr_url')
    if not tr_url.endswith('/'):
        tr_url += '/'
    url = tr_url + 'index.php?/api/v2/'
    user = context.config.userdata.get('tr_user')
    password = context.config.userdata.get('tr_passwd')


def __send_request(method, uri, data):
    url_uri = url + uri
    request = urllib.request.Request(url_uri)
    if method == 'POST':
        request.data = bytes(json.dumps(data), 'utf-8')
    auth = str(
        base64.b64encode(
            bytes('%s:%s' % (user, password), 'utf-8')
        ),
        'ascii'
    ).strip()
    request.add_header('Authorization', 'Basic %s' % auth)
    request.add_header('Content-Type', 'application/json')

    e = None
    try:
        response = urllib.request.urlopen(request).read()
    except urllib.error.HTTPError as ex:
        response = ex.read()
        e = ex

    if response:
        result = json.loads(response.decode())
    else:
        result = {}

    if e is not None:
        if result and 'error' in result:
            error = '"' + result['error'] + '"'
            print(error)
        else:
            error = 'No additional error message received'
            print(error)

    return result

#
# Send Get
#
# Issues a GET request (read) against the API and returns the result
# (as Python dict).
#
# Arguments:
#
# uri                 The API method to call including parameters
#                     (e.g. get_case/1)
#


def send_get(uri):
    return __send_request('GET', uri, None)


#
# Send POST
#
# Issues a POST request (write) against the API and returns the result
# (as Python dict).
#
# Arguments:
#
# uri                 The API method to call including parameters
#                     (e.g. add_case/1)
# data                The data to submit as part of the request (as
#                     Python dict, strings must be UTF-8 encoded)
def send_post(uri, data):
    return __send_request('POST', uri, data)


def update_testrail(case_id, run_id, result_flag, msg=""):
    """Update TestRail for a given run_id and case_id
    update_flag = False
    Update the result in TestRail using send_post function.
    Parameters for add_result_for_case is the combination of runid and case id.
    status_id is 1 for Passed, 2 For Blocked, 4 for Retest and 5 for Failed"""
    try:
        log.teardown("Context empty", 'info', "Updating the test result at Testrail")
        status_id = 1 if result_flag is True else 5
        result = None
        if run_id is not None and case_id != 'None':
            try:
                send_post('add_result_for_case/%s/%s' % (run_id, case_id),
                          {'status_id': status_id, 'comment': msg})
            except (eh.wrapping_exceptions()) as e:
                eh.get_error_details(sys.exc_info(), e)
            else:
                return print('Updated test result for case: %s in test run: %s\n' % (case_id, run_id))

        return result
    except (eh.wrapping_exceptions()) as e:
        pass
        log.teardown("Context empty", 'error', "Cannot update the testplan " + str(e))


def get_project_id(project_name):
    """Get the project ID using project name"""
    project_id = None
    projects = send_get("get_projects/{}".format(project_name))
    for project in projects:
        if project['name'] == project_name:
            project_id = project['id']
            break
    return project_id


def get_suite_id(project_name, suite_name):
    """Get the suite ID using project name and suite name"""
    suite_id = None
    project_id = get_project_id(project_name)
    suites = send_get('get_suites/{}'.format(project_id))
    for suite in suites:
        if suite['name'] == suite_name:
            suite_id = suite['id']
            break
    return suite_id


def get_milestone_id(project_name, milestone_name):
    """Get the milestone ID using project name and milestone name"""
    milestone_id = None
    project_id = get_project_id(project_name)
    milestones = send_get('get_milestones/{}'.format(project_id))
    for milestone in milestones:
        if milestone['name'] == milestone_name:
            milestone_id = milestone['id']
            break
    return milestone_id


def get_user_id(user_name):
    """Get the user ID using user name"""
    user_id = None
    users = send_get('get_users')
    for user in users:
        if user_name == user['name']:
            user_id = user['id']
            break
    return user_id


def get_run_id(project_name, test_run_name):
    """Get the run ID using test name and project name"""
    run_id = None
    project_id = get_project_id(project_name)
    try:
        test_runs = send_get('get_runs/{}'.format(project_id))
    except Exception as e:
        print(str(e))
        #eh.get_error_details(sys.exc_info(), e)
    else:
        for test_run in test_runs:
            if test_run['name'] == test_run_name:
                run_id = test_run['id']
                break
    return run_id


def create_milestone(project_name, milestone_name, milestone_description=""):
    """Create a new milestone if it does not already exist"""
    milestone_id = get_milestone_id(project_name, milestone_name)
    if milestone_id is None:
        project_id = get_project_id(project_name)
        if project_id is not None:
            try:
                data = {'name': milestone_name,
                        'description': milestone_description}
                send_post('add_milestone/{}'.format(str(project_id)),
                          data)
            except Exception as e:
                eh.get_error_details(sys.exc_info(), e)
            else:
                print('Created the milestone: {}'.format(milestone_name))
    else:
        print("Milestone '{}' already exists".format(milestone_name))


def create_new_project(new_project_name, project_description, show_announcement, suite_mode):
    """Create a new project if it does not already exist"""
    project_id = get_project_id(new_project_name)
    if project_id is None:
        try:
            send_post('add_project',
                      {'name': new_project_name,
                       'announcement': project_description,
                       'show_announcement': show_announcement,
                       'suite_mode': suite_mode, })
        except Exception as e:
            eh.get_error_details(sys.exc_info(), e)
    else:
        print("Project already exists {}".format(new_project_name))


def create_testplan(project_name, test_run_name, milestone_name=None, description="", suite_name=None,
                    case_ids=None, assigned_to=None):
    """Create a new test run if it does not already exist"""
    # reference: http://docs.gurock.com/testrail-api2/reference-runs

    if case_ids is None:
        case_ids = []
    project_id = get_project_id(project_name)
    test_run_id = get_run_id(project_name, test_run_name)
    if project_id is not None and test_run_id is None:
        data = {}
        if suite_name is not None:
            suite_id = get_suite_id(project_name, suite_name)
            if suite_id is not None:
                data['suite_id'] = suite_id
        data['name'] = test_run_name
        data['description'] = description
        if milestone_name is not None:
            milestone_id = get_milestone_id(project_name, milestone_name)
            if milestone_id is not None:
                data['milestone_id'] = milestone_id
        if assigned_to is not None:
            assignedto_id = get_user_id(assigned_to)
            if assignedto_id is not None:
                data['assignedto_id'] = assignedto_id
        if len(case_ids) > 0:
            data['case_ids'] = case_ids
            data['include_all'] = False

        try:
            send_post('add_run/{}'.format(project_id), data)
        except Exception as e:
            eh.get_error_details(sys.exc_info(), e)
        else:
            print('Created the test run: %s' % test_run_name)
    else:
        if project_id is None:
            print("Cannot add test run %s because Project %s was not found" % (test_run_name, project_name))
        elif test_run_id is not None:
            print("Test run '%s' already exists" % test_run_name)


def delete_project(new_project_name, project_description):
    """Delete an existing project"""
    project_id = get_project_id(new_project_name)
    if project_id is not None:
        try:
            send_post('delete_project/{}'.format(project_id), project_description)
        except Exception as e:
            eh.get_error_details(sys.exc_info(), e)
    else:
        print('Cant delete the project given project name: {}'.format(new_project_name))


def delete_test_run(test_run_name, project_name):
    """Delete an existing test run"""
    run_id = get_run_id(test_run_name, project_name)
    if run_id is not None:
        try:
            send_post('delete_run/{}'.format(run_id), test_run_name)
        except Exception as e:
            eh.get_error_details(sys.exc_info(), e)
    else:
        print('Cant delete the test run for given project and test run name: {} , {}'.format(project_name,
                                                                                             test_run_name))


def create_testplan_name(name):
    today = datetime.date.today()
    tr_name = "TestRun: {} {}".format("Version {} - ".format(name), today.strftime('%d, %b %Y'))
    return tr_name
