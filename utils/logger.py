import logging
from utils import directory_handler


def teardown(context, type, data):
    try:
        message = build_message(context, data)
        directory_handler.move_from_feaures_or_configs_dir_to("output")
        context, handler = create_logging_with_context(context) if context != "Context empty" else create_logging_with_no_context(context)
        attach_data_to_logger(type, message, context)
        clear_context_logger_buffer(context, handler)
    except Exception as e:
        print('Problem with the logger teardown\n' + str(e))


def create_logging_with_no_context(context):
    global logger, handler
    if context == "Context empty":
        logger = logging.getLogger('seleniumframework')
        logger.logger = False
        logger.setLevel(logging.DEBUG)
        handler = logging.FileHandler('./seleniumframework_logger.log')
        formatter = logging.Formatter('%(asctime)s %(levelname)s %(message)s')
        handler.setFormatter(formatter)
        logger.addHandler(handler)
    return logger, handler


def create_logging_with_context(context):
    context.logger = logging.getLogger('seleniumframework')
    context.logger.setLevel(logging.DEBUG)
    handler = logging.FileHandler('./seleniumframework_logger.log')
    formatter = logging.Formatter('%(asctime)s %(levelname)s %(message)s')
    handler.setFormatter(formatter)
    context.logger.addHandler(handler)
    return context, handler


def attach_data_to_logger(type, message, context):
    try:
        if context.logger is not False:
            if type == 'info':
                context.logger.info(message)
            elif type == 'warning':
                context.logger.warning(message)
            elif type == 'error':
                context.logger.error(message)
        else:
            if type == 'info':
                context.info(message)
            elif type == 'warning':
                context.warning(message)
            elif type == 'error':
                context.error(message)
    except Exception as e:
        print(str(e))


def build_message(context, data):
    if context == "Context empty":
        message = data
    else:
        message = data + ", User: " + context.config.userdata['user'] + \
                ", Browser: " + context.config.userdata['browser']
    return message


def clear_context_logger_buffer(context, handler):
    try:
        if context.logger is not False:
            context.logger.removeHandler(handler)
            context.logger = None
        else:
            context.removeHandler(handler)
    except Exception as e:
        print(str(e))
