from datetime import datetime
import json
import subprocess
from urllib.parse import urlparse
import pandas as pd
import glob
import matplotlib.pyplot as plt
import pylab, time, sys

import utils.directory_handler as dh


class ShellError(Exception):
    pass


def paser_url(url):
    o = urlparse(url.replace(" ", ""))
    hostname = o.hostname
    result_name = o.path[1:].replace('/', '_')
    return hostname, result_name


def execute_lighthouse(url, browser):
    hostname, result_name = paser_url(url)

    flags = {browser == "mobile": '--chrome-flags="--ignore-certificate-errors --headless --no-sandbox"',
             browser == "desktop": '--chrome-flags="--ignore-certificate-errors --headless --no-sandbox" --emulated-form-factor=none'}.get(True)

    dh.move_from_project_dir_to('output/lighthouse/results')
    cmd = ['lighthouse {} '
           '--output=json --output-path={}_{}_{}.json {}'.format(
        url,
        browser,
        hostname,
        result_name,
        flags
    )]
    status = subprocess.call(cmd, shell=True)
    if status != 0:
        raise ShellError


def create_csv_report(browser, hostname, csv_filename):
    dh.move_from_project_dir_to("output/lighthouse/results")
    scores = list()

    report_filenames = glob.glob('{}_{}*.json'.format(browser, hostname))

    for report_filename in report_filenames:
        score = get_score_from_json(report_filename)
        scores.append(score)

    df = pd.DataFrame(scores)
    dh.move_from_project_dir_to("output/lighthouse/csv")
    df.to_csv(csv_filename, index=False)
    return df


def get_score_from_json(report_filename):
    dh.move_from_project_dir_to("output/lighthouse/results")
    score = dict()
    with open(report_filename, 'r') as desc:
        content_json = json.load(desc)
    score['url'] = content_json.get('finalUrl', None)
    date = content_json.get('fetchTime', None)
    if date:
        date = datetime.strptime(date, '%Y-%m-%dT%H:%M:%S.%fZ')
        score['date'] = datetime.strftime(date, '%Y-%m-%d')

    for category, value in content_json.get('categories').items():
        score[category] = 0 if value.get("score") is None else value.get("score") * 100
    return score


def create_image_summary_report(browser, report_name):
    dh.move_from_project_dir_to("output/lighthouse/csv")
    df = pd.read_csv(browser + "_" + report_name + ".csv")

    plt.style.use('seaborn-darkgrid')
    df.plot.line(x='url', style='-o', legend=True)  # draw lines from X using url values # style -o draws the points
    plt.legend(loc='center left', bbox_to_anchor=(1.0, 0.5))

    plt.xticks(rotation=90)  # X "bottom" titles position
    pylab.ylim([0, 100])  # graph value range

    # Add titles
    plt.title(browser + " - " + report_name, loc='left', fontsize=12, fontweight=0, color='orange')
    plt.ylabel('Lighthouse Percentage Results')  # x graph report title
    plt.xlabel("Urls")

    # plt.yticks(rotation=0)  # Y "side" titles position
    dh.move_from_project_dir_to("output/lighthouse/reports")
    date = datetime.now().strftime("%B %d, %Y - %H:%M%p")
    pylab.savefig("{}_{} - {}.png".format(browser, report_name, date),
                  additional_artists=[],
                  bbox_inches="tight")