from utils import test_rail as tr
from selenium import webdriver
from elementium.drivers.se import SeElements
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities


# browser general options should be set at base.py
def setup_browser(context):
    if 'browser' in context.config.userdata.keys():
        browser = context.config.userdata.get('browser')
        if browser is None:
            context.browser = SeElements(webdriver.Chrome())
        elif browser == "phantomjs":
            context.browser = SeElements(webdriver.PhantomJS())
        elif browser == "chrome":
            context.browser = SeElements(webdriver.Chrome())
        elif browser == "chrome_h":
            chrome_options = webdriver.ChromeOptions()
            chrome_options.add_argument("headless")
            chrome_options.add_argument('--no-sandbox')
            chrome_options.add_argument('--allow-running-insecure-content')
            chrome_options.add_argument('--ignore-certificate-errors')
            chrome_options.add_argument("--window-size=1920,1200")

            capabilities = DesiredCapabilities.CHROME.copy()
            capabilities['acceptSslCerts'] = True
            capabilities['acceptInsecureCerts'] = True

            context.browser = SeElements(webdriver.Chrome(chrome_options=chrome_options, desired_capabilities=capabilities))
    else:
        print("browser var does not exist",
              "Please configure a browser var within: userconfig.json - " +
              "behave.ini or using -D flag")
    return context.browser


def clean_browser(context):
    try:
        context.browser.browser.quit()
        context.browser = None
    except Exception as e:
        print(str(e))


def get_result_status(scenario):
    message = ""
    for index, step in enumerate(scenario.steps):
        a = scenario.steps[index].name + '\n\n'
        b = scenario.steps[index].captured.output
        c = scenario.steps[index].error_message
        message += a + b + c.split("Captured",1)[0] if c is not None else ""
    return message


def get_project_data(context):
    try:
        project = context.config.userdata.get('project')
        version = context.config.userdata.get('version')
        dev = context.config.userdata.get('dev')
        return project, version , dev

    except Exception as e:
        print(str(e))


def get_run_id(context, test_plan_name):
    run_id = tr.get_run_id(context.config.userdata.get('project'), test_plan_name) if \
        context.config.userdata.get('dev') == "no" else context.config.userdata.get('testplan_run_id')
    return run_id

