import os
import shutil
import time
from utils.logger import teardown

import utils.directory_handler


def shutil_function():
    shutil.make_archive(time.strftime("%d_%m_%Y"),
                        'zip', "failed_scenarios_screenshots")
    shutil.move(time.strftime("%d_%m_%Y") + ".zip",
                "failed_scenarios_zip_runs/" +
                time.strftime("%d_%m_%Y") + ".zip")
    shutil.rmtree("failed_scenarios_screenshots")


def zip_screenshots(context):
    teardown(context, 'warning', "Zipping screenshots")
    try:
        if os.path.exists("failed_scenarios_screenshots"):
            utils.directory_handler.create_dir_failed_scenarios()
            shutil_function()
        else:
            print('There is no screenshots to zip\n')
    except Exception as e:
        teardown(context, 'error', "Problem with zip_screenshots" + str(e))


def create_screenshots(context, scenario):
    try:
        if scenario.status == "failed":
            teardown(context, 'warning', "Creating screenshot")
            utils.directory_handler.move_from_feaures_or_configs_dir_to("output")
            utils.directory_handler.move_from_output_dir_to_failed_scenarios_screenshots()
            context.browser.browser.save_screenshot(scenario.name +
                                                    ".feature_failed.png")
            utils.directory_handler.move_from_failed_scenarios_screenshots_dir_to__output()
        else:
            teardown(context, 'info',
                            "failed_scenarios_screenshots dir already exists")
    except Exception as e:
        utils.directory_handler.move_from_failed_scenarios_screenshots_dir_to__output()
        teardown(context, 'error', "Problem with create_screenshots " + str(e))