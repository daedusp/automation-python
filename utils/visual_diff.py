import utils.directory_handler as dh


def visual_diff(context):
    import subprocess
    image_name = '{}'.format(str(context.tc_id))
    file_v1 = get_source_file_path("base", image_name)
    file_v2 = get_source_file_path("current", image_name)
    output = get_output_file_path("failed_scenarios_screenshots", image_name)
    return subprocess.run(["blink-diff", "--output", output,
                           file_v1,
                           file_v2], stdout=subprocess.PIPE).returncode


def get_source_file_path(dir_name, image_name):
    return dh.get_project_path() + "/output/screenshot_diff/{}/{}.png".format(dir_name, image_name)


def get_output_file_path(dir_name, image_name):
    return dh.get_project_path() + "/output/{}/{}vd.png".format(dir_name, image_name)