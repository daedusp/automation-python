import sys, os
import libs.pages.bot_style as bot

sys.path.insert(0, "")
from . import base
import libs.locators.header_locators as header_locators
from utils import exceptions_handler


class HeaderPage(base.BasePage):
    def __init__(self, browser):
        super(HeaderPage, self).__init__(browser)
        self.HeaderLocators = header_locators.HeaderLocators

    def search(self, item):
        try:
            bot.search(self.browser, self.HeaderLocators.search_textbox, self.HeaderLocators.search_button, item)
        except (exceptions_handler.wrapping_exceptions()) as e:
            exceptions_handler.get_error_details(sys.exc_info(), e)
            return 'Error the search process'
