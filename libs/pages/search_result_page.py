import sys, os
import libs.pages.bot_style as bot

sys.path.insert(0, "")
from . import base
import libs.locators.search_result_page_locators as search_result_page
from utils import exceptions_handler


class SearchResultPage(base.BasePage):
    def __init__(self, browser):
        super(SearchResultPage, self).__init__(browser)
        self.GSLocators = search_result_page.GoogleSearchLocators

    def search(self, text):
        try:
            bot.search(self.browser, self.GSLocators.search_textbox, self.GSLocators.search_button, text)
        except (exceptions_handler.wrapping_exceptions()) as e:
            exceptions_handler.get_error_details(sys.exc_info(), e)
            return 'Error when trying to search'