class GoogleSearchLocators():
    search_button = {
        "css": ".FPdoLc [name='btnK']",
        "xpath": "(//*/div[@class='suggestions-inner-container'])[2]"
    }

    search_textbox = {
        "css": "[maxlength]",
        "xpath": "//*/input[@role='combobox']"
    }

    search_result ={
        "xpath": "//*/h3[contains(.,'{}')]"
    }
