class HeaderLocators():
    search_textbox = {
        "xpath": "//*//input[@name='search']"
    }

    search_button = {
        "xpath": "//*//span[@class='input-group-btn']"
    }

    search_result ={
        "xpath": "//*/h3[contains(.,'{}')]"
    }

