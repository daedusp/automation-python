automation-python

# BDD + Page Object Pattern + Selenium DSL
# Selenium webdriver
# Elementium
# Behave
# Google Lighthouse Insights

# Tested on Python 3.6.4
# PEP 8 convention

# Important Links  
[Behave Main Project](https://github.com/behave/behave)   
[Behave self Reference](http://pythonhosted.org/behave/api.html#)
[What is behave](http://www.seleniumframework.com/python-basic/what-is-behave/)     
[Python Behave Selenium Page Object Pattern](https://github.com/machzqcq/Python_Page_Object)    
[Behave example step by step***](https://jenisys.github.io/behave.example/index.html )   
[Python Behave example using selenium webdriver(POM)](https://gist.github.com/carlosmmelo/0ca23e212dcd98028baa)   
[Python integration of selenium webdriver and behave bdd](https://github.com/accraze/webdriver-behave-example)   
[Selenium Webdriver Object DSL > Elementium](https://github.com/actmd/elementium)       
[Allure Reports](https://docs.qameta.io/allure/2.0/#_behave )   
[Logger](https://fangpenlin.com/posts/2012/08/26/good-logging-practice-in-python/ )   
[Logger](http://stackoverflow.com/questions/6386698/using-the-logging-python-class-to-write-to-a-file)
[PyHamcrest Matcher](https://github.com/hamcrest/PyHamcrest)
[Lighthouse - a tool that lets you audit your web application based on a number of parameters including (but not limited to) performance, based on a number of metrics, mobile compatibility, Progressive Web App (PWA) implementations, etc.](https://developers.google.com/web/tools/lighthouse/)
[blink-diff - A lightweight image comparison tool by Yahoo](http://yahoo.github.io/blink-diff/)

Not implemented
[Parallel Issue](https://github.com/behave/behave/issues/235)   
[Parallel workaround](https://github.com/tokunbo/behave-parallel/tree/upstreamsync)   
[Parallel demo](https://github.com/vishalm/behave_parallel_demo)

# Website
Get this repository using [ Automation Framework ](https://gitlab.com/daedusp/automation-python)  


# Usage
Pre-requisites:
Install by pip requirements.txt

1. If you do not know how to use git, click "Download zip" option at right corner
2. cd to feature dir
3. Manully > behave --tags=@regression
4. CI behave --tags=@regression -D url=="http://tutorialsninja.com/demo/index.php?route=common/home"
for CI you will need to use > browser, url, url_api and user, check configs/userconfig.json to check original values

"behave" command to run all feature files in the given folder
"behave automation_test.feature" to run the automation_test.feature file only

5. Run behave with "-v" gives verbose log. For example running headless using phantomjs with -v creates ghostdriver.log

Project_name
├── configs_dir # contains hardcoded strings and settings
|   ├── defaults.yaml # default configuration values to be overriden by the environment
|   └── __init__.py # config loader
├── features
|   ├── steps_dir # contains all test steps
|   ├── behave.py # contains hardcoded behave strings and settings
|   ├── environment.py # Setup/Teardown behave environment . Includes all hooks.
├── libs_dir # contains POO Structure: pages + locators, handlers, and other scripts
|   ├── locators
|   ├── pages
|   ├──  __init__.py # config loader
├── output # contains logs, screenshots, zips and any necessary results from the TCs
├── utils # contains handlers and other scripts
|   ├── requirements.txt # list of project package requirements
├── __init__.py # config loader
├── .gitignore # list of files to ignore when pushing to github
├── README.md # readme file with basic Documentation
