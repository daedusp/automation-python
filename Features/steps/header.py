import sys
from hamcrest import assert_that, equal_to, is_not, is_, contains_string, not_

sys.path.append('../libs')
from libs.pages import *


@step(u'I search for "{item}"')
def step_impl(context, item):
    context.page = PageBuilder("Header")(context.browser)
    context.page.search(item)
    #assert_that(context.browser.browser.title, is_(equal_to("Your Store")))