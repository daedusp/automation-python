from hamcrest import assert_that, equal_to, is_


@step(u'I navigate to home page')
def step_impl(context):
    context.login_url=context.config.userdata.get('url')
    context.browser.navigate(context.login_url)
    assert_that(context.browser.browser.title, is_(equal_to("Your Store")))


@step(u'Im running the following test case: "{tc_id}"')
def step_given_i_setup_tc_id(context, tc_id):
    context.tc_id = int(tc_id)