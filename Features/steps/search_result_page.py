from hamcrest import assert_that, equal_to, is_



@step(u'I open first item from result page')
def step_impl(context):
    context.login_url=context.config.userdata.get('url')
    context.browser.navigate(context.login_url)


@step('I see "{search_result}"')
def step_i_check_a_result(context, search_result):
    context.page = PageBuilder("GoogleSearch")(context.browser)
    assert_that(context.page.check_text_result(search_result), is_(equal_to(search_result)))


