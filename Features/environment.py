import sys
import os

sys.path.append(os.path.dirname(os.path.dirname(os.path.realpath(__file__))))
from utils import utils, userdata, exceptions_handler as eh, logger as log, test_rail as tr, screenshot_handler as sh

test_plan_name = ""


def before_all(context):
    try:
        log.teardown("Context empty", 'info', "Before all: Getting userdata.json information")
        context = userdata.loading_userdata_from_json(context)
        dev, project, version = utils.get_project_data(context)

        global test_plan_name
        test_plan_name = tr.create_testplan_name(version)

        if dev == "no":
            log.teardown(context, 'info', "Creating TestRail TestPlan")
            tr.create_testplan(project, test_plan_name, milestone_name=None, description="", suite_name=None,
                               case_ids=context.config.userdata.get('tc_automated'), assigned_to=None)

    except (eh.wrapping_exceptions()) as e:
        log.teardown(context, 'error', "Before feature " + str(e))


def before_feature(context, feature):
    log.teardown(context, 'info', "Before feature")


def before_scenario(context, scenario):
    log.teardown(context, 'info', "Before scenario")
    utils.setup_browser(context)


def after_scenario(context, scenario):
    try:
        log.teardown(context, 'info', "After Scenario C{} with Status: ".format(str(context.tc_id)) +
                     scenario.status.name)
        dev, project, version = utils.get_project_data(context)
        sh.create_screenshots(context, scenario)
        flag = True if scenario.status.name == "passed" else False
        utils.clean_browser(context)

        if dev == "no":
            tr.set_tr_config(context)
            run_id = utils.get_run_id(context, test_plan_name)
            tr.update_testrail(context.tc_id, run_id, flag, utils.get_result_status(scenario))

    except (eh.wrapping_exceptions()) as e:
        log.teardown("Context empty", 'error', "After Scenario " + str(e))
        return eh.get_error_details(sys.exc_info(), e)


def after_feature(context, feature):
    log.teardown("Context empty", 'info', "After feature")


def after_all(context):
    try:
        log.teardown("Context empty", 'info', "After all")
        sh.zip_screenshots("Context empty")
    except (eh.wrapping_exceptions()) as e:
        print(e)
        log.teardown("Context empty", 'error', "After all " + str(e))
